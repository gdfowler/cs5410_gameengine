let canvas = null
let ctx = null

let continueGameLoop = true

let startTime = 0
let time = 0
let prevTime = 0

let x = 200
let y = 10

let backgroundImg = new Image()
backgroundImg.src = 'img/background.jpeg'
backgroundImg.ready = false
backgroundImg.onload = () => backgroundImg.ready = true

let fireBallImg = new Image()
fireBallImg.src = 'img/fireball.png'
fireBallImg.ready = false
fireBallImg.onload = () => fireBallImg.ready = true

let particleSystem = ParticleSystem(drawingService, {
  image: fireBallImg,
  center: { x: 300, y: 300 },
  size: { mean: 10, stdev: 3 },
  speed: { mean: 0, stdev: 0.01 },
  lifetime: { mean: 10000, stdev: 250 }
})

let myCharacter = (imgPath => {
  let that = {}
  that.center = { x: 200, y: 200 }
  that.reset = () => that.center = { x: 200, y: 200 }
  that.characterImg = new Image()
  that.characterImg.src = imgPath
  that.characterImg.ready = false
  that.characterImg.onload = () => that.characterImg.ready = true

  that.render = () => {
    drawingService.drawTexture(
      that.characterImg,
      that.center,
      0,
      { x: 100, y: 100 }
    )
  }

  that.moveUp = (elapsedTime) => {
    that.center.y = that.center.y - (elapsedTime / 10)
  }
  that.moveDown = (elapsedTime) => {
    that.center.y = that.center.y + (elapsedTime / 10)
  }
  that.moveLeft = (elapsedTime) => {
    that.center.x = that.center.x - (elapsedTime / 10)
  }
  that.moveRight = (elapsedTime) => {
    that.center.x = that.center.x + (elapsedTime / 10)
  }

  return that
})("img/cardinalCharacter.png")


function initializeKeybindings() {
  let currKeybindings = JSON.parse(localStorage.getItem('keybindings'))
  // currKeybindings.forEach(e => { console.log(e) })
  for (var e in currKeybindings) {
    if (e === 'up')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.moveUp)
    if (e === 'down')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.moveDown)
    if (e === 'left')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.moveLeft)
    if (e === 'right')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.moveRight)
  }
}

function initialize() {
  initializeKeybindings()
  continueGameLoop = true
  // get canvas
  canvas = document.getElementById('canvas')
  ctx = canvas.getContext('2d')
  drawingService.init(document.getElementById('canvas'))

  // set up event listeners
  window.addEventListener('keydown', (event) => {
    inputEvents.addToBuffer(event)
  })
  window.addEventListener('keyup', (event) => {
    inputEvents.removeFromBuffer(event)
  })

  // init fps counter
  fpsCounter.init()
  fpsCounter.updateTimeStart()
  startTime = performance.now()

  // start the game loop
  requestAnimationFrame(gameLoop)
}

function endGameLoop() {
  continueGameLoop = false
}

function processInput(elapsedTime) {
  inputEvents.processInput(elapsedTime)
}

function update(timestamp) {
  fpsCounter.incrementCounter()
  fpsCounter.updateTimeNow(timestamp)
  particleSystem.update(timestamp)
}

function render() {
  drawingService.clear()
  drawingService.drawBackground(backgroundImg)
  ctx.fillStyle = 'green';
  drawingService.drawOutlineSquare(10, 10, 150, 150)
  drawingService.drawFillSquare(x, y, 100, 100)
  drawingService.strokeText('Test', 10, 200, undefined, '30px serif')
  drawingService.fillText('Test', 10, 250, undefined, '30px serif')
  drawingService.drawCircle(450, 120, 100)
  drawingService.drawArc(250, 250, 100, 0, Math.PI)
  myCharacter.render()
  // particleSystem.render()
  fpsCounter.render()
}

function gameLoop(timestamp) {
  elapsedTime = timestamp - prevTime
  prevTime = timestamp
  processInput(elapsedTime)
  update(timestamp)
  render()

  if (continueGameLoop)
    requestAnimationFrame(gameLoop)
  else {
    inputEvents.unregisterAll()
    myCharacter.reset()
  }
}