let isShowingHighScores = false
let isShowingCredits = false
let isShowingKeybindings = false

let blackListKeys = ['Escape']

let keybindings = {
  up: 'w',
  down: 's',
  left: 'a',
  right: 'd'
}

localStorage.setItem('keybindings', JSON.stringify(keybindings))

window.addEventListener('keydown', (event) => {
  if (event.key === 'Escape') {
    exitToMenu()
  }
})

function showHighScores() {
  if (isShowingHighScores) {
    document.getElementById('highscores-parent').style = 'display: none;'
    document.getElementById('highscores-button').innerHTML = 'View High Scores'
    isShowingHighScores = false
  } else {
    document.getElementById('highscores-parent').style = 'display: block;'
    document.getElementById('highscores-button').innerHTML = 'Hide High Scores'
    isShowingHighScores = true
  }
}

function showCredits() {
  if (isShowingCredits) {
    document.getElementById('credits').style = 'display: none;'
    document.getElementById('credits-button').innerHTML = 'View Credits'
    isShowingCredits = false
  } else {
    document.getElementById('credits').style = 'display: block;'
    document.getElementById('credits-button').innerHTML = 'Hide Credits'
    isShowingCredits = true
  }
}

function showKeybindings() {
  if (isShowingKeybindings) {
    document.getElementById('keybindings').style = 'display: none;'
    document.getElementById('keybindings-button').innerHTML = 'Set Keybindings'
    isShowingKeybindings = false
  } else {
    document.getElementById('keybindings').style = 'display: block;'
    document.getElementById('keybindings-button').innerHTML = 'Hide Keybindings'
    isShowingKeybindings = true
  }
}

function menuStartGame() {
  document.getElementById('menu').style = "display: none;"
  document.getElementById('gameArea').style = "display: block;"
  initialize()
  // Start the game here
}

function exitToMenu() {
  document.getElementById('menu').style = "display: block;"
  document.getElementById('gameArea').style = "display: none;"
  endGameLoop()
  // End the game here
}

let currAction = undefined
function setKeybindings(action) {
  currAction = action
}

window.addEventListener('keypress', (event) => {
  if (blackListKeys.indexOf(event.key) == -1 && currAction != undefined) {
    keybindings[currAction] = event.key
    currAction = undefined
    updateKeybindings()
  }
})

function updateKeybindings() {
  localStorage.setItem('keybindings', JSON.stringify(keybindings))
  document.getElementById('up-key').innerHTML = keybindings['up']
  document.getElementById('down-key').innerHTML = keybindings['down']
  document.getElementById('left-key').innerHTML = keybindings['left']
  document.getElementById('right-key').innerHTML = keybindings['right']
}