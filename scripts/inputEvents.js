let inputEvents = (() => {
  let that = {
    handlers: {},
    inputBuffer: {}
  }

  that.registerCommand = (key, handler) => {
    that.handlers[key] = handler
  }

  that.unregisterCommand = (key) => {
    if (that.handlers[key]) {
      delete that.handlers[key]
    }
  }

  that.addToBuffer = (event) => {
    that.inputBuffer[event.key] = event.key
  }

  that.removeFromBuffer = (event) => {
    if (that.inputBuffer[event.key]) {
      delete that.inputBuffer[event.key]
    }
  }

  that.processInput = (elapsedTime) => {
    for (key in that.inputBuffer) {
      if (that.handlers[key]) {
        that.handlers[key](elapsedTime)
      }
    }
  }

  that.unregisterAll = () => {
    that.handlers = {}
  }

  return that
})()