let fpsCounter = (() => {
  let that = {}
  let counter = 0
  let time_start = 0
  let time_now = 0
  let fpsHTML = null
  let timeHTML = null

  that.init = () => {
    fpsHTML = document.getElementById('fps')
    timeHTML = document.getElementById('timestamp')
    counter = 0
    time_start = 0
    time_now = 0
  }

  that.incrementCounter = () => {
    counter += 1
  }

  that.updateTimeStart = () => {
    time_start = performance.now() / 1000
  }

  that.updateTimeNow = (time) => {
    time_now = time / 1000
  }

  that.render = () => {
    fpsHTML.innerHTML = Math.floor((counter / (time_now - time_start)) * 100) / 100
    timeHTML.innerHTML = Math.round(time_now - time_start)
  }

  return that
})()